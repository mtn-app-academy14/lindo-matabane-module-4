import 'package:flutter/material.dart';
import 'dashboard_page.dart';
import 'registration_page.dart';


class LoginPage extends StatelessWidget {
  @override
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amberAccent,
      appBar: AppBar(
        title: Text("Login", style: TextStyle(color: Colors.black)),
        elevation: .1,
        backgroundColor: Colors.orange,
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        onPressed: () {
            Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => 
            LoginPage()),);
          },
        ),
      ),
      body: SingleChildScrollView(
          child: Column(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Text(
            "African Recipes",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 25.00,
              fontWeight: FontWeight.w600,
            ),
          ),
          Image.asset("assets/images/two.png", scale: 0.1),
          Text(
            "Healthy Food For Your Roots",
            textAlign: TextAlign.center,
            style: TextStyle(
              fontFamily: "Times New Roman",
              fontSize: 20.00,
              fontWeight: FontWeight.w500,
            ),
          ),
          Padding(
            padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
            child: TextFormField(
                decoration: const InputDecoration(
                border: UnderlineInputBorder(),
                labelText: 'Enter your username',
              ),
            ),
          ),
          Padding(
          padding: const EdgeInsets.symmetric(horizontal: 8, vertical: 16),
          child: TextFormField(
            decoration: const InputDecoration(
              border: UnderlineInputBorder(),
              labelText: 'Enter your password',
            ),
          ),
        ),
          RaisedButton(
              child: Text('Login'),
              color: Color.fromARGB(255, 90, 89, 89),
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                Dashboard()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 90.0,
                vertical: 5.0,
              ),),
          RaisedButton(
              child: Text('Registration'),
              color: Color.fromARGB(255, 90, 89, 89),
              textColor: Colors.white,
              onPressed: () {
                Navigator.of(context).push(
                MaterialPageRoute(builder: (context) => 
                RegistrationPage()),);
              },
              padding: EdgeInsets.symmetric(
                horizontal: 70.0,
                vertical: 5.0,
              )),
        ],
      )),
    );
  }
}
