import 'package:flutter/material.dart';
import 'package:font_awesome_flutter/font_awesome_flutter.dart';
import '/screens/feature_page1.dart';
import '/screens/feature_page2.dart';
import '/screens/profile_page.dart';
import '/screens/login_page.dart';

class Dashboard extends StatelessWidget {
  Widget build(BuildContext context) {
    return Scaffold(
      backgroundColor: Colors.amberAccent,
      appBar: AppBar(
        backgroundColor: Colors.orange,
        title: Text("Dashboard", style: TextStyle(color: Colors.black)),
        leading: IconButton(
          icon: Icon(
            Icons.arrow_back,
            color: Colors.black,
          ),
        onPressed: () {
            Navigator.of(context).push(
            MaterialPageRoute(builder: (context) => 
            Dashboard()),);
          },
        ),
      ),
      body: Center(
          child: Column(
        children: <Widget>[
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 150.0,
                    minWidth: 150.0,
                    color: Colors.red,
                    textColor: Colors.black,
                    child: new FaIcon(FontAwesomeIcons.bowlFood, size: 50,),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FeatureOne()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 150.0,
                    minWidth: 150.0,
                    color: Colors.red,
                    textColor: Colors.black,
                    child: new Icon(Icons.emoji_food_beverage, size: 50,),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => FeatureTwo()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
            ],
          ),
          Row(
            mainAxisAlignment: MainAxisAlignment.center,
            children: <Widget>[
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 150.0,
                    minWidth: 150.0,
                    color: Colors.red,
                    textColor: Colors.black,
                    child: new Icon(Icons.person, size: 50,),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => SettingsUI()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  )),
              Padding(
                  padding: EdgeInsets.all(20.0),
                  child: new MaterialButton(
                    height: 150.0,
                    minWidth: 150.0,
                    color: Colors.red,
                    textColor: Colors.black,
                    child: new Icon(Icons.logout, size: 50,),
                    onPressed: () => {
                    Navigator.push(
                    context,
                    MaterialPageRoute(builder: (context) => LoginPage()),
                    )
                    },
                    splashColor: Colors.redAccent,
                  ),
                  ),
            ],
          ),

        ],
      )),
    );
  }
}
